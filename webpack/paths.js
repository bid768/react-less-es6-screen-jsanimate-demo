const path = require('path')
const ROOT_PATH = path.resolve(__dirname, '..')
const OUTPUT_PATH = path.resolve(ROOT_PATH, './build')
const SOURCE_PATH = path.resolve(ROOT_PATH, './src')
const OUTPUT_ASSETS_PATH = path.resolve(ROOT_PATH, './build/assets')

module.exports = {
    ROOT_PATH,
    OUTPUT_PATH,
    SOURCE_PATH,
    OUTPUT_ASSETS_PATH
}
