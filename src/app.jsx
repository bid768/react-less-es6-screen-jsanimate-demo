import React from 'react'
import ReactDOM from 'react-dom'
import { Router, Route } from 'react-router-dom'

// import moment from 'moment';
// import 'moment/locale/zh-cn';

import HomePage from 'modules/index'
import 'styles/base.css'

// 引入组件

import Lvjing from 'modules/lvjing/lvjing.jsx'
const createHistory = require('history').createBrowserHistory

class App extends React.Component {
    render() {
        return (
            <Router history={createHistory()}>
                <Route
                    render={() => {
                        return (
                            <HomePage>
                                <Route path="/" exact component={Lvjing} />

                                <Route path="/lvjing" component={Lvjing} />
                            </HomePage>
                        )
                    }}
                ></Route>
            </Router>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('root'))
