import React, { Component } from 'react'
import styles from './lvjing.less'
import demoImg from 'img/effect.jpg'

export default class Lvjing extends Component {
    constructor(props) {
        super(props)
        this.state = {
            imgUrl: '',
            activeCls: ''
        }
        this.uploadFlag = false
    }

    addImg() {
        const files = this.insertImg.files
        this.insertImg(files)
    }

    insertImg(files) {
        const reader = new FileReader()
        const self = this
        if (files && files[0]) {
            reader.readAsDataURL(files[0])
            reader.onload = function() {
                const imgUrl = this.result
                self.setState({ imgUrl })
                self.uploadFlag = true
                self.insertImg.value = '' // 解决onchange事件之执行一次的问题
            }
        } else {
            alert('图片获取失败')
        }
    }

    close() {
        if (this.uploadFlag) {
            this.uploadFlag = false
            this.setState({ imgUrl: '' })
        }
    }

    handleDragOver(e) {
        e = e || window.event
        e.preventDefault()
        e.stopPropagation()
    }

    handleDrop(e) {
        e = e || window.event
        e.preventDefault()
        e.stopPropagation()
        var files = e.dataTransfer.files || e.target.files
        // 先清空之前的img
        this.preview.getAttribute('src') && this.preview.removeAttribute('src')
        this.insertImg(files)
    }

    getBlock() {
        var props = this.props.filterProp
        console.log(demoImg)
        const item =
            props &&
            props.map((item, index) => {
                return (
                    <button
                        key={'filter-item-' + index}
                        onClick={this.filter.bind(this, item.name)}
                    >
                        <div>
                            <img
                                className={
                                    styles.btnImg + ' ' + styles[item.name]
                                }
                                src={demoImg}
                                width="50px"
                            />
                        </div>
                        <p>{item.value}</p>
                    </button>
                )
            })
        return item
    }

    filter(cls) {
        this.setState({ activeCls: cls })
    }

    render() {
        const { imgUrl, activeCls } = this.state
        const previewShow = imgUrl ? 'inline-block' : 'none'

        return (
            <div className={styles.filterWrapper}>
                <div
                    className={styles.fileArea}
                    onDrop={e => this.handleDrop(e)}
                    onDragOver={e => this.handleDragOver(e)}
                >
                    <a className={styles.file}>
                        <p>支持拖拽上传</p>
                        <input
                            className={styles.inputImg}
                            type="file"
                            accept="image/*"
                            ref={node => (this.insertImg = node)}
                            onChange={this.addImg.bind(this)}
                        />
                        <div className={styles.imgDiv}>
                            <img
                                ref={node => (this.preview = node)}
                                style={{ display: previewShow }}
                                className={
                                    styles.preview + ' ' + styles[activeCls]
                                }
                                src={imgUrl}
                                width="100%"
                            />
                        </div>
                        <i
                            className={styles.close}
                            style={{ display: previewShow }}
                            title="关闭"
                            onClick={this.close.bind(this)}
                        >
                            关闭
                        </i>
                    </a>
                </div>
                <div className={styles.btnGroup}>{this.getBlock()}</div>
                <canvas className={styles[activeCls]} />
            </div>
        )
    }
}
Lvjing.defaultProps = {
    filterProp: [
        { name: 'normal', value: '原图' },
        { name: 'black', value: '黑白经典' },
        { name: 'old', value: '旧时光' },
        { name: 'pink', value: '阳光派对' },
        { name: 'roll', value: '胶卷' },
        { name: 'sun', value: '红粉佳人' },
        { name: 'japen', value: '日系清新' },
        { name: 'beautiful', value: '唯美风' },
        { name: 'dark', value: '灰暗风' },
        { name: 'blurry', value: '模糊世界' },
        { name: 'invert', value: '反转色' }
    ]
}
