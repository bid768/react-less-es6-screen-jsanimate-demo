import React from 'react'
import { NavLink } from 'react-router-dom'

import styles from './index.less'

class HomePage extends React.Component {
    render() {
        return (
            <div className={styles.container}>
                <h1>react组件库</h1>
                <div className={styles.navContent}>
                    <div className={styles.navBox}>
                        <NavLink
                            to="/lvjing"
                            activeClassName={styles.linkActive}
                        >
                            滤镜
                        </NavLink>
                    </div>
                    <div className={styles.contentBox}>
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}
export default HomePage
